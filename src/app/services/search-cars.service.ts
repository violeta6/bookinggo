import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Location, LocationsResponse } from '../models/rental-car.model';

@Injectable({ providedIn: 'root' })
export class SearchCarsService {
  private endpointUrl = 'https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en';

  constructor(private http: HttpClient) { }

  /**
   * GET rental cars whose name contains search term
   * @param searchTerm search term
   */
  searchCars(searchTerm: string): Observable<Location[]> {
    if (!searchTerm.trim() || searchTerm.length < 2) {
      return of([]);
    }

    return this.http.get<LocationsResponse>(`${this.endpointUrl}&solrRows=6&solrTerm=${searchTerm}`)
    .pipe(
      map(data => data.results.docs),
      catchError(this.handleError<any[]>('searchCars', []))
    );
  }

  /**
   * Handle Http operation error
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: Add better error handling
      alert(`${operation} failed: ${error.message}`);

      // return empty result
      return of(result as T);
    };
  }
}
