import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { SearchCarsService } from './search-cars.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('SearchCarsService', () => {
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                SearchCarsService,
            ],
            imports: [
                HttpClientTestingModule
            ]
        });

        httpMock = TestBed.get(HttpTestingController);
    });

    it('should return locations list items', inject([SearchCarsService], (service: SearchCarsService) => {
        const locations =  [
            {
                alternative: ['GB,UK,England,Manchester Airport'],
                bookingId: 'airport-38566',
                city: 'Manchester',
                country: 'United Kingdom',
                countryIso: 'gb',
                iata: 'MAN',
                isPopular: false,
                lang: 'en',
                lat: 53.3536,
                lng: -2.27472,
                locationId: '38566',
                name: 'Manchester Airport',
                placeKey: '1472187',
                placeType: 'A',
                region: 'Greater Manchester',
                searchType: 'L',
                ufi: 900038550,
            }
        ];

        spyOn(service, 'searchCars').and
            .returnValue(of(locations));

        service.searchCars('manchester').subscribe(data => {
            expect(data).toEqual(locations);
        });
    }));
});
