export interface LocationsResponse {
    results: LocationsData;
}

export interface LocationsData {
    isGooglePowered: boolean;
    docs: Location[];
}

export interface Location {
    country: string;
    lng: number;
    city: string;
    searchType: string;
    alternative: string[];
    bookingId: string;
    placeType: string;
    placeKey: string;
    iata: string;
    countryIso: string;
    locationId: string;
    name: string;
    ufi: number;
    isPopular: boolean;
    region: string;
    lang: string;
    last?: number;
}

