import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { Location } from '../../models/rental-car.model';
import { SearchCarsService } from '../../services/search-cars.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: [ './search.component.css' ]
})
export class SearchComponent implements OnInit {
  locations$: Observable<Location[]>;

  private searchTerms = new Subject<string>();

  constructor(private searchCarsService: SearchCarsService) {}

  ngOnInit(): void {
    this.locations$ = this.searchTerms
    .pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => {
        return this.searchCarsService.searchCars(term);
      }),
    );
  }

  search(term: string): void {
    this.searchTerms.next(term);
  }
}
