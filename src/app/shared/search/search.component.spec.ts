import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';

import { Location } from '../../models/rental-car.model';
import { SearchCarsService } from '../../services/search-cars.service';
import { SearchComponent } from './search.component';

export class SearchCarsServiceMock {
    searchCars(): Observable<Location[]> {
        return of([{
            alternative: ['GB,UK,England,Manchester Airport'],
            bookingId: 'airport-38566',
            city: 'Manchester',
            country: 'United Kingdom',
            countryIso: 'gb',
            iata: 'MAN',
            isPopular: false,
            lang: 'en',
            lat: 53.3536,
            lng: -2.27472,
            locationId: '38566',
            name: 'Manchester Airport',
            placeKey: '1472187',
            placeType: 'A',
            region: 'Greater Manchester',
            searchType: 'L',
            ufi: 900038550,
        }]);
    }
}

describe('SearchComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        SearchComponent
      ],
      providers: [{
          provide: SearchCarsService,
          useClass: SearchCarsServiceMock
      }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  it('should create SearchComponent', () => {
    const fixture = TestBed.createComponent(SearchComponent);
    const component = fixture.debugElement.componentInstance;

    expect(component).toBeTruthy();
  });

  it('Given that search() has been trigggered, searchTerms should be defined', () => {
    const fixture = TestBed.createComponent(SearchComponent);
    const component = fixture.debugElement.componentInstance;

    fixture.detectChanges();
    component.search('manchester');

    expect(component.locations$).toBeDefined();

    component.locations$.subscribe(locations => {
        expect(locations.length).toBe(1);
    });
  });

});
